import "./index_header.scss";
import { useEffect, useState } from "react";
import Cookies from "js-cookie";
import { Avatar, Dropdown, message, Menu, Button, notification, Modal, Row, Col } from "antd";
import { LogoutOutlined, SettingOutlined, SmileOutlined, GlobalOutlined, EllipsisOutlined, CloseOutlined, UserOutlined } from "@ant-design/icons";
import { useNavigate } from "react-router-dom";

const Header = (props) => {
  const navigate = useNavigate();

  const handleMenuUserClick = (e) => {
    switch (e.key) {
      case "1":
        Cookies.remove("username");
        notification.open({
          message: "Logout",
          description: "Logout Success",
          icon: <SmileOutlined style={{ color: "green" }} />,
        });
        navigate("/login");
        break;
      case "3":
        navigate("/admin");
        break;
      case "2":
        alert("Change password");
        break;
      default:
        throw new Error("invalid case");
    }
  };
  const handleMenuLanguageClick = (e) => {
    switch (e.key) {
      case "1":
        notification.open({
          message: "Tiếng Việt",
          description: "Đã đổi ngôn ngữ sang Tiếng Việt",
          icon: <SmileOutlined style={{ color: "green" }} />,
        });
        break;
      case "2":
        notification.open({
          message: "English",
          description: "Language changed to English",
          icon: <SmileOutlined style={{ color: "green" }} />,
        });
        break;
      default:
        throw new Error("invalid case");
    }
  };
  const menuUser = (
    <Menu
      onClick={handleMenuUserClick}
      items={[
        {
          label: "LogOut",
          key: "1",
          icon: <LogoutOutlined />,
        },
        {
          label: "Change Password",
          key: "2",
          icon: <SettingOutlined />,
        },
        {
          label: "Admin Page",
          key: "3",
          icon: <UserOutlined />,
        },
      ]}
    />
  );
  const menuLanguage = (
    <Menu
      onClick={handleMenuLanguageClick}
      items={[
        {
          label: "Tiếng Viêt",
          key: "1",
        },
        {
          label: "English",
          key: "2",
        },
      ]}
    />
  );
  const [isShowModal, setShowModal] = useState(false);
  const showHideModal = () => {
    setShowModal(!isShowModal);
  };

  const [open, setOpen] = useState(false);
  return (
    <div className="container">
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <div className="responsiveLogo">
          <h1 className="logoHeader">
            <a className="navbar-brand" href="/">
              <span className="text-under-logo">Tri thức trực tuyến</span>
            </a>
          </h1>

          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav mr-auto">
              <li className="nav-item">
                <a className="nav-link" href="/xuat-ban">
                  Xuất bản
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="/sach">
                  Sách
                </a>
              </li>

              <li className="nav-item">
                <a className="nav-link" href="/kinh-doanh">
                  Kinh doanh
                </a>
              </li>

              <li className="nav-item">
                <a className="nav-link" href="#">
                  Công nghệ
                </a>
              </li>

              <li className="nav-item">
                <a className="nav-link" href="#">
                  Thể thao
                </a>
              </li>

              <li className="nav-item">
                <a className="nav-link" href="#">
                  Đời sống
                </a>
              </li>

              <li className="nav-item">
                <a className="nav-link" href="#">
                  Lifestyle
                </a>
              </li>

              <li className="nav-item-show" onClick={() => showHideModal()}>
                <div>
                  {isShowModal ? (
                    <CloseOutlined style={{ fontSize: "24px", fontWeight: "bold", verticalAlign: "middle" }} onClick={() => setOpen(true)} />
                  ) : (
                    <EllipsisOutlined style={{ fontSize: "30px", fontWeight: "bold", verticalAlign: "middle" }} onClick={() => setOpen(true)} />
                  )}
                </div>
              </li>
            </ul>
          </div>
        </div>
        <form className="form-inline my-2 my-lg-0">
          <Dropdown overlay={menuUser} placement="bottomLeft">
            <div className="userSelector">
              <p>{props.username}</p>
              <Avatar src="https://joeschmoe.io/api/v1/random" />
            </div>
          </Dropdown>
          <Dropdown overlay={menuLanguage} placement="bottomRight">
            <GlobalOutlined style={{ fontSize: "24px" }} />
          </Dropdown>
        </form>
      </nav>
      <Modal
        visible={isShowModal}
        onOk={() => setShowModal(false)}
        onCancel={() => setShowModal(false)}
        footer={null}
        closable={false}
        mask={false}
        bodyStyle={{
          backgroundColor: "#141329",
        }}
        getContainer
        //style={{ height: "100px" }}
      >
        <Row gutter={[16, 24]}>
          <Col className="modalColumn" span={6}>
            <p className="subTitle"> Xuất Bản </p>
            <ul>
              <li>Tin tức xuất bản</li>
              <li>Sách hay</li>
              <li>Nghiên cứu xuất bản</li>
              <li>Tác giả</li>
            </ul>
          </Col>

          <Col className="modalColumn" span={6}>
            <p className="subTitle"> Kinh doanh </p>
            <ul>
              <li>Bất động sản</li>
              <li>Tiền Của Tôi</li>
              <li>Tiêu dùng</li>
              <li>Thông tin doanh nghiệp</li>
            </ul>
          </Col>
          <Col className="modalColumn" span={6}>
            <p className="subTitle"> Công nghệ </p>
            <ul>
              <li>Blockchain</li>
              <li>Mobile</li>
              <li>Gadget</li>
              <li>Internet</li>
            </ul>
          </Col>
          <Col className="modalColumn" span={6}>
            <p className="subTitle"> Thể thao</p>
            <ul>
              <li>Bóng đá Việt Nam</li>
              <li>Bóng đá Anh</li>
              <li>Võ thuật</li>
              <li>eSports</li>
              <li>Dinh dưỡng Thể thao</li>
            </ul>
          </Col>

          <Col className="modalColumn" span={6}>
            <p className="subTitle"> Giải trí </p>
            <ul>
              <li>Sao</li>
              <li>Âm nhạc </li>
              <li>Phim ảnh</li>
              <li>Thời trang</li>
            </ul>
          </Col>
          <Col className="modalColumn" span={6}>
            <p className="subTitle"> Đời sống </p>
            <ul>
              <li>Giới trẻ</li>
              <li>Xu hướng </li>
              <li>Sống đẹp</li>
              <li>Gender</li>
              <li>Sự kiện</li>
            </ul>
          </Col>
          <Col className="modalColumn" span={6}>
            <p className="subTitle">Sách hay</p>
            <ul>
              <li>Ebook</li>
              <li>Audiobook </li>
            </ul>
          </Col>
          <Col className="modalColumn" span={6}>
            <p className="subTitle"> Tác giả</p>
          </Col>
        </Row>
      </Modal>
    </div>
  );
};
const style = {
  background: "#0092ff",
  padding: "8px 0",
};
export default Header;
