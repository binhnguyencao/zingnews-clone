import { Form, Input, Button, Checkbox, notification } from 'antd';
import { SmileOutlined } from '@ant-design/icons';
import { useNavigate } from 'react-router-dom'

import Cookies from 'js-cookie'

import { connect } from "react-redux"
import { compose, mapProps } from "recompose";
import './index.css'
import React from 'react';
import { createStructuredSelector } from 'reselect'

import { asyncLoginAuthenticationAction } from './store/actions';


const Login = (props) => {
    const { loginAuthenticate } = props
    const onFail = () => {
        notification.open({
            message: 'Login',
            description:
                'Login Failed',
            icon: <SmileOutlined style={{ color: 'red' }} />,
        });
    }

    const navigate = useNavigate()
    const onFinish = async (values) => {
        console.log(values)
        const res = await loginAuthenticate(values)
        console.log(res)
        if (res === null) {
            onFail()
        }
        else if (res.data.code === 200) {
            notification.open({
                message: 'Login',
                description:
                    'Login Success',
                icon: <SmileOutlined style={{ color: 'green' }} />,
            });

            navigate("/")
            Cookies.set('username', res.data.result.username,)
            //  Cookies.set('username', res.data.result.username, { expires: 1 / 500 })

        }

        console.log('Success:', res);
    };

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };

    return (
        <Form
            name="basic"
            labelCol={{
                span: 8,
            }}
            wrapperCol={{
                span: 8,
            }}
            initialValues={{
                remember: true,
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
        >

            <Form.Item
                label="Username"
                name="username"
                rules={[
                    {
                        required: true,
                        message: 'Please input your email!',
                    },
                ]}
            >
                <Input />
            </Form.Item>

            <Form.Item
                label="Password"
                name="password"
                rules={[
                    {
                        required: true,
                        message: 'Please input your password!',
                    },
                ]}
            >
                <Input.Password />
            </Form.Item>



            <Form.Item
                wrapperCol={{
                    offset: 8,
                    span: 16,
                }}
            >
                <Button type="primary" htmlType="submit">
                    Submit
                </Button>
            </Form.Item>
        </Form>

    );
};



const mapStateToProps = createStructuredSelector({


})
const mapDispatchtoProps = (dispatch) => ({
    loginAuthenticate: (payload) => asyncLoginAuthenticationAction(dispatch)(payload),

})

const withConnect = connect(mapStateToProps, mapDispatchtoProps)
export default compose(withConnect)(Login);