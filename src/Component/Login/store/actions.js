import {
    SET_LOADING_STEP,
    LOGIN_AUTHENTICATION,
    SAVE_TOKEN_LOGIN
} from './constants';


export function setLoadingStep(payload) {
    return {
        type: SET_LOADING_STEP,
        payload
    }
}


export function saveTokenLogin(payload) {
    return {
        type: SAVE_TOKEN_LOGIN,
        payload
    }
}

export const asyncLoginAuthenticationAction = (dispatch) => (payload) =>
    new Promise((resolve) =>
        dispatch({ type: LOGIN_AUTHENTICATION, payload, resolve })
    );

