import { takeLatest, put, call } from 'redux-saga/effects'

import { setLoadingStep, saveTokenLogin } from './actions'

import { LOGIN_AUTHENTICATION } from './constants'

import { loginService } from '../../../Services/apiLogin'



function* LoginAuthenticationSaga({ payload, resolve }) {
    yield put(setLoadingStep(true))
    try {
        const response = yield call(loginService, payload)
        resolve(response)

        yield put(saveTokenLogin(response.data.result.token))
        yield put(setLoadingStep(false))

    }
    catch (error) {
        resolve(null)
        yield put(setLoadingStep(false))
    }
}


export function* sagaLogin() {
    yield takeLatest(LOGIN_AUTHENTICATION, LoginAuthenticationSaga)
}