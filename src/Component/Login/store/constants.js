const LOGIN = "login"

const LOGIN_AUTHENTICATION = `${LOGIN}/authentication`

const SAVE_TOKEN_LOGIN = `${LOGIN}/saveToken`

const SET_LOADING_STEP = `${LOGIN}/setLoadingStep`


export {
    LOGIN,
    LOGIN_AUTHENTICATION,
    SET_LOADING_STEP,
    SAVE_TOKEN_LOGIN
}