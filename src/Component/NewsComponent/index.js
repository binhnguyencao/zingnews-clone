import { useNavigate } from 'react-router-dom'
import './index.scss'


const NewsComponent = (props) => {
    const { type, alt, src, title, description, id } = props
    return (
        type === 1 ?
            <div className='type1Main'>
                <div className="type1" >
                    <a href={"/page/" + id}><img src={src} alt={alt} ></img></a>
                    <a href={"/page/" + id}>  <p className='titleType' >{title}</p></a>
                </div >
                <hr></hr>
            </div>
            :
            type === 2 ? <div className="type2" >
                < a href={"/page/" + id}><img src={src} alt={alt} ></img></a>
                <a href={"/page/" + id}><p className='titleType' >{title}</p></a>
                <p className='type2Des'>{description}</p>
            </div > :
                <div className="type3">
                    < a href={"/page/" + id}><img src={src} alt={alt} ></img></a>
                    <a href={"/page/" + id}> <p className='titleType' >{title}</p></a>
                </div >
    );
}
export default NewsComponent
