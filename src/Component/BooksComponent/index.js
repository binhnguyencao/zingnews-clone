
import './index.scss'

const BooksComponent = (props) => {

    const { type, alt, src, title, description, id } = props
    return (
        type === 1 ?
            <div>
                <div className="type1Book" >
                    <a href={"/sach/" + id}><img src={src} alt={alt} ></img></a>
                    <a href={"/sach/" + id}>  <p className='titleType' >{title}</p></a>
                </div >

            </div>
            :
            type === 2 ? <div className="type2Book" >
                < a href={"/sach/" + id}><img src={src} alt={alt} ></img></a>
                <a href={"/sach/" + id}><p className='titleType' >{title}</p></a>
                <p>{description}</p>
            </div > :
                <div className="type3Book">
                    < a href={"/sach/" + id}><img src={src} alt={alt} ></img></a>
                    <a href={"/sach/" + id}> <p className='titleType' >{title}</p></a>
                </div >
    );
}
export default BooksComponent


