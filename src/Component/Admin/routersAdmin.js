import React from "react"

import News from "./pages/News"
import Books from "./pages/Books"


const routersAdmin = [
    {
        path: "/news",
        component: () => <News />,
    },
    {
        path: "/books",
        component: () => <Books />,
    },

]
export default routersAdmin