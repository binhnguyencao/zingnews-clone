import { Table, Space, Modal, Form, Input, InputNumber, Button, notification, Select, Switch } from "antd"
import { PlusCircleOutlined, DeleteOutlined, EditOutlined, PlusOutlined } from "@ant-design/icons"

import { useEffect, useState } from "react"

import { createStructuredSelector } from 'reselect'


import { selectAllNews, selectLoading } from "../store/selectors";
import { getAllNewsAction, createNewsAction, getDetailNewsAction, updateNewsAction, deleteNewsAction } from "../store/actions";
import { connect } from "react-redux"
import { compose, mapProps } from "recompose";

const { TextArea } = Input
const { Option } = Select
const News = (props) => {


    const [form] = Form.useForm()

    const [isShowKey, seIsSHowKey] = useState(true);



    const { getAllNewsDispatch, createNewsDispatch, getDetailNewsDispatch, updateNewsDispatch, deleteNewsDispatch } = props
    const { listNews, isLoading } = props
    const [urlImage, setUrlImage] = useState()

    const urlImageChange = (e) => {
        setUrlImage(e.target.value)
    }
    const currentTime = new Date()
    useEffect(() => {
        getAllNewsDispatch()

    }, [])


    const onDelete = async (id) => {
        const response = await deleteNewsDispatch(id);
        if (response === 200) {
            await getAllNewsDispatch()
        }
    }

    const onEdit = async (id) => {
        seIsSHowKey(true)
        const response = await getDetailNewsDispatch(id);
        if (response.status === 200) {
            form.setFieldsValue({
                title: response.data.title,
                type: response.data.type,
                description: response.data.description,
                content: response.data.content,
                image: response.data.image,
                id: response.data.id,
            })
            setIsModalVisible(true);
        }
    }

    const [isModalVisible, setIsModalVisible] = useState(false);

    const handleShowing = async (value, record) => {
        const temp = { ...record, showing: !value }
        console.log(temp)
        const response = await updateNewsDispatch({ id: temp.id, data: temp })
        if (response.status === 200) {
            getAllNewsDispatch()
        }


    }
    const handleOk = () => {
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);

    };
    const onSubmit = async (values) => {

        if (isShowKey) {
            const response = await updateNewsDispatch({ id: values.id, data: values })
            if (response.status === 200) {
                getAllNewsDispatch()
            }
            form.resetFields();
            handleCancel();
        }
        else {
            const response = await createNewsDispatch({ ...values, showing: false })
            if (response.status === 201) {
                await getAllNewsDispatch()
            }

            notification.success({
                message: 'Add news success',
            });
            form.resetFields();
            handleCancel();
        }

    }
    const columns = [
        {
            title: 'Title',
            dataIndex: 'title',
            key: 'title',
            render: (text) => <a href={text}>{text}</a>,

        },
        {
            title: 'Type',
            dataIndex: 'type',
            key: 'type',

        },
        {
            title: 'Description',
            dataIndex: 'description',
            key: 'description',
        },
        {
            title: 'Content',
            dataIndex: 'content',
            key: 'content',
        },
        {
            title: 'Image',
            dataIndex: 'image',
            key: 'image',
            render: (value) => <img src={value} alt={value} width={200} ></img>
        },
        {
            title: 'Action',
            key: 'action',
            render: (record) => (
                <Space size="middle">
                    <DeleteOutlined onClick={() => onDelete(record.id)} style={{ fontSize: '24px', color: 'red' }} />

                    <EditOutlined onClick={() => onEdit(record.id)} style={{ fontSize: '24px', color: '#40a9ff' }} />
                </Space >
            ),
        },
        {
            title: 'Showing',
            dataIndex: 'showing',
            key: 'showing',
            render: (value, record) => (<Switch defaultChecked={value} onChange={() => handleShowing(value, record)} />),
        },
        {
            title: 'Create At',
            dataIndex: 'createtime',
            key: 'createtime',
            defaultSortOrder: ['ascend', 'descend', 'ascend'],
            sorter: (a, b) => a.createtime - b.createtime,
            render: (value) => <p >{caculateTime(value)} ago</p>
        },
    ]
    const caculateTime = (seconds) => {
        seconds = currentTime.getTime() / 1000 - seconds / 1000
        seconds = Number(seconds);
        if (seconds <= 0) {
            return '0 second ago'
        }
        else {
            var d = Math.floor(seconds / (3600 * 24));
            var h = Math.floor(seconds % (3600 * 24) / 3600);
            var m = Math.floor(seconds % 3600 / 60);
            var s = Math.floor(seconds % 60);

            var dDisplay = d > 0 ? d + (d === 1 ? " day, " : " days, ") : "";
            var hDisplay = h > 0 ? h + (h === 1 ? " hour, " : " hours, ") : "";
            var mDisplay = m > 0 ? m + (m === 1 ? " minute, " : " minutes, ") : "";
            var sDisplay = s > 0 ? s + (s === 1 ? " second" : " seconds") : "";
            return dDisplay + hDisplay + mDisplay + sDisplay;
        }
    };
    const createStudent = () => {
        console.log(props)
        setIsModalVisible(true);
        seIsSHowKey(false)
    };
    function setModalName(isShowKey) {
        if (isShowKey) {
            return "EDIT NEWS"
        }
        else return "CREATE NEWS"
    }
    return (
        <div>
            <br></br>

            <Button onClick={() => createStudent()} icon={<PlusCircleOutlined />} style={{ background: "#73d13d" }}>
                Create News
            </Button>
            <br></br>
            <Table loading={isLoading} columns={columns} dataSource={listNews} />
            <Modal title={setModalName(isShowKey)} visible={isModalVisible} onOk={handleOk} onCancel={handleCancel} footer={
                <Space>
                    <Button onClick={handleCancel}>Cancel</Button>
                    <Button type='primary' htmlType='submit' form='form'>OK</Button>
                </Space>
            }>
                <Form
                    form={form}
                    name="form"
                    wrapperCol={{
                        span: 24,
                    }}
                    initialValues={{
                        remember: true,
                    }}
                    onFinish={onSubmit}
                    autoComplete="off"
                >

                    <Form.Item label="ID" name="id">
                        <Input disabled={true} />
                    </Form.Item>

                    <Form.Item
                        label="Title"
                        name="title"
                        rules={[
                            {
                                required: true,
                                message: "Please input news title!",
                            },
                        ]}
                    >
                        <TextArea showCount maxLength={50} />
                    </Form.Item>
                    <Form.Item
                        label="Type"
                        name="type"
                        rules={[
                            {
                                required: true,
                                message: "Please input news type",
                            }
                        ]}
                    >
                        <Select style={{ width: "100%", }}>
                            <Option value={1}>1</Option>
                            <Option value={2}>2</Option>
                            <Option value={3}>3</Option>
                        </Select>
                    </Form.Item>

                    <Form.Item
                        label="Description"
                        name="description"
                        rules={[
                            {
                                required: true,
                                message: "Please input news description!",
                            },
                        ]}
                    >
                        <TextArea showCount maxLength={200} />
                    </Form.Item>

                    <Form.Item
                        label="Content"
                        name="content"
                        rules={[
                            {
                                required: true,
                                message: "Please input news content",
                            }
                        ]}
                    >
                        <TextArea showCount maxLength={500} />
                    </Form.Item>
                    <Form.Item label="Image"
                        name="image">
                        <Input onChange={(e) => urlImageChange(e)} />

                    </Form.Item>
                    <img src={urlImage} alt={urlImage} style={imgStyle}></img>

                </Form>
            </Modal>
        </div >
    )
};
const imgStyle = {
    width: "300px",
    display: "block",
    marginLeft: "auto",
    marginRight: "auto"
}

const mapStateToProps = createStructuredSelector({
    listNews: selectAllNews,
    isLoading: selectLoading,
})
const mapDispatchtoProps = (dispatch) => ({
    getAllNewsDispatch: (payload) => dispatch(getAllNewsAction(payload)),

    createNewsDispatch: (payload) => createNewsAction(dispatch)(payload),

    getDetailNewsDispatch: (payload) => getDetailNewsAction(dispatch)(payload),

    updateNewsDispatch: (payload) => updateNewsAction(dispatch)(payload),

    deleteNewsDispatch: (payload) => deleteNewsAction(dispatch)(payload),
})

const withConnect = connect(mapStateToProps, mapDispatchtoProps)
export default compose(withConnect)(News);