import { Table, Space, Modal, Form, Input, InputNumber, Button, notification, Select, Switch } from "antd"
import { PlusCircleOutlined, DeleteOutlined, EditOutlined, PlusOutlined } from "@ant-design/icons"

import { useEffect, useState } from "react"

import { createStructuredSelector } from 'reselect'

import { selectAllBooks, selectLoading } from "../store/selectors";
import { getAllBooksAction, createBooksAction, getDetailBooksAction, updateBooksAction, deleteBooksAction } from "../store/actions";
import { connect } from "react-redux"
import { compose, mapProps } from "recompose";

const { TextArea } = Input
const { Option } = Select
const Books = (props) => {

    const [form] = Form.useForm()

    const [isShowKey, seIsSHowKey] = useState(true);


    const { getAllBooksDispatch, createBooksDispatch, getDetailBooksDispatch, updateBooksDispatch, deleteBooksDispatch } = props
    const { listBooks, isLoading } = props

    const [urlImage, setUrlImage] = useState()
    const urlImageChange = (e) => {
        setUrlImage(e.target.value)
    }

    useEffect(() => {
        getAllBooksDispatch()
        console.log(listBooks)
    }, [])

    const onDelete = async (id) => {
        const response = await deleteBooksDispatch(id);
        if (response === 200) {
            getAllBooksDispatch()
        }
    }

    const onEdit = async (id) => {
        seIsSHowKey(true)
        const response = await getDetailBooksDispatch(id);
        if (response.status === 200) {
            form.setFieldsValue({
                title: response.data.title,
                type: response.data.type,
                description: response.data.description,
                content: response.data.content,
                image: response.data.image,
                id: response.data.id,
            })
            setIsModalVisible(true);
        }
    }

    const [isModalVisible, setIsModalVisible] = useState(false);

    const handleShowing = async (value, record) => {
        const temp = { ...record, showing: !record.showing }
        console.log(temp)
        const response = await updateBooksDispatch({ id: temp.id, data: temp })
        if (response.status === 200) {
            getAllBooksDispatch()
        }

    }
    const handleOk = () => {
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);

    };
    const onSubmit = async (values) => {

        if (isShowKey) {
            const response = await updateBooksDispatch({ id: values.id, data: values })
            if (response.status === 200) {
                getAllBooksDispatch()
            }
            form.resetFields();
            handleCancel();
        }
        else {
            const response = await createBooksDispatch(values)
            if (response.status === 201) {
                getAllBooksDispatch()
            }

            notification.success({
                message: 'Add books success',
            });
            form.resetFields();
            handleCancel();
        }

    }
    const columns = [
        {
            title: 'Title',
            dataIndex: 'title',
            key: 'title',
            render: (text) => <a href={text}>{text}</a>,
        },
        {
            title: 'Type',
            dataIndex: 'type',
            key: 'type',
        },
        {
            title: 'Description',
            dataIndex: 'description',
            key: 'description',
        },
        {
            title: 'Content',
            dataIndex: 'content',
            key: 'content',
        },
        {
            title: 'Image',
            dataIndex: 'image',
            key: 'image',
            render: (value) => <img src={value} alt={value} width={200} ></img>
        },
        {
            title: 'Action',
            key: 'action',
            render: (record) => (
                <Space size="middle">
                    <DeleteOutlined onClick={() => onDelete(record.id)} style={{ fontSize: '24px', color: 'red' }} />

                    <EditOutlined onClick={() => onEdit(record.id)} style={{ fontSize: '24px', color: '#40a9ff' }} />
                </Space >
            ),
        },
        {
            title: 'Showing',
            dataIndex: 'showing',
            key: 'showing',
            render: (value, record) => (<Switch defaultChecked={value} onChange={() => handleShowing(value, record)} />),
        },
    ]
    const createStudent = () => {
        console.log(props)
        setIsModalVisible(true);
        seIsSHowKey(false)
    };
    function setModalName(isShowKey) {
        if (isShowKey) {
            return "EDIT BOOKS"
        }
        else return "CREATE BOOKS"
    }
    return (
        <div>
            <br></br>

            <Button onClick={() => createStudent()} icon={<PlusCircleOutlined />} style={{ background: "#73d13d" }}>
                Create Books
            </Button>
            <br></br>
            <Table loading={isLoading} columns={columns} dataSource={listBooks} />
            <Modal title={setModalName(isShowKey)} visible={isModalVisible} onOk={handleOk} onCancel={handleCancel} footer={
                <Space>
                    <Button onClick={handleCancel}>Cancel</Button>
                    <Button type='primary' htmlType='submit' form='form'>OK</Button>
                </Space>
            }>
                <Form
                    form={form}
                    name="form"
                    wrapperCol={{
                        span: 24,
                    }}
                    initialValues={{
                        remember: true,
                    }}
                    onFinish={onSubmit}
                    autoComplete="off"
                >

                    <Form.Item label="ID" name="id">
                        <Input disabled={true} />
                    </Form.Item>

                    <Form.Item
                        label="Title"
                        name="title"
                        rules={[
                            {
                                required: true,
                                message: "Please input books title!",
                            },
                        ]}
                    >
                        <TextArea showCount maxLength={50} />
                    </Form.Item>
                    <Form.Item
                        label="Type"
                        name="type"
                        rules={[
                            {
                                required: true,
                                message: "Please input books type",
                            }
                        ]}
                    >
                        <Select style={{ width: "100%", }}>
                            <Option value={1}>1</Option>
                            <Option value={2}>2</Option>
                            <Option value={3}>3</Option>
                        </Select>
                    </Form.Item>

                    <Form.Item
                        label="Description"
                        name="description"
                        rules={[
                            {
                                required: true,
                                message: "Please input books description!",
                            },
                        ]}
                    >
                        <TextArea showCount maxLength={200} />
                    </Form.Item>

                    <Form.Item
                        label="Content"
                        name="content"
                        rules={[
                            {
                                required: true,
                                message: "Please input books content",
                            }
                        ]}
                    >
                        <TextArea showCount maxLength={500} />
                    </Form.Item>
                    <Form.Item label="Image"
                        name="image">
                        <Input onChange={(e) => urlImageChange(e)} />

                    </Form.Item>
                    <img src={urlImage} alt={urlImage} style={imgStyle}></img>
                </Form>
            </Modal>
        </div >
    )
};
const imgStyle = {
    width: "300px",
    display: "block",
    marginLeft: "auto",
    marginRight: "auto"
}

const mapStateToProps = createStructuredSelector({
    listBooks: selectAllBooks,
    isLoading: selectLoading,
})
const mapDispatchtoProps = (dispatch) => ({
    getAllBooksDispatch: (payload) => dispatch(getAllBooksAction(payload)),

    createBooksDispatch: (payload) => createBooksAction(dispatch)(payload),

    getDetailBooksDispatch: (payload) => getDetailBooksAction(dispatch)(payload),

    updateBooksDispatch: (payload) => updateBooksAction(dispatch)(payload),

    deleteBooksDispatch: (payload) => deleteBooksAction(dispatch)(payload),
})

const withConnect = connect(mapStateToProps, mapDispatchtoProps)
export default compose(withConnect)(Books);

