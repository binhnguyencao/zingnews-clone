import './index.scss'
import Cookies from 'js-cookie'
import { Avatar, Dropdown, message, Menu, Button, notification, Modal } from 'antd';
import { LogoutOutlined, SettingOutlined, SmileOutlined, GlobalOutlined, ReadOutlined, BookOutlined, KeyOutlined } from '@ant-design/icons';
import { useNavigate } from 'react-router-dom'
import { Link } from 'react-router-dom';
import { useState } from 'react';
const AdminHeader = (props) => {

    const navigate = useNavigate()

    const handleMenuUserClick = (e) => {
        switch (e.key) {
            case "1":
                Cookies.remove('username')
                notification.open({
                    message: 'Logout',
                    description:
                        'Logout Success',
                    icon: <SmileOutlined style={{ color: 'green' }} />,
                });
                navigate("/login")
                break;
            case "2":
                alert("Change password")
                break;
            default: throw new Error("invalid case")
        }
    };
    const handleMenuLanguageClick = (e) => {
        switch (e.key) {
            case "1":
                notification.open({
                    message: 'Tiếng Việt',
                    description:
                        'Đã đổi ngôn ngữ sang Tiếng Việt',
                    icon: <SmileOutlined style={{ color: 'green' }} />,
                });
                break;
            case "2":
                notification.open({
                    message: 'English',
                    description:
                        'Language changed to English',
                    icon: <SmileOutlined style={{ color: 'green' }} />,
                });
                break;
            default: throw new Error("invalid case")
        }
    };
    const menuUser = (
        <Menu
            onClick={handleMenuUserClick}
            items={[
                {
                    label: 'LogOut',
                    key: '1',
                    icon: <LogoutOutlined />,
                },
                {
                    label: 'Change Password',
                    key: '2',
                    icon: <SettingOutlined />,
                },
            ]}
        />
    );
    const menuLanguage = (
        <Menu
            onClick={handleMenuLanguageClick}
            items={[
                {
                    label: 'Tiếng Viêt',
                    key: '1',

                },
                {
                    label: 'English',
                    key: '2',

                },
            ]}
        />
    );

    const [open, setOpen] = useState(false);


    return (
        <div className='header'>
            <Menu mode="horizontal" >
                <Menu.Item key="" icon={<SettingOutlined style={{ fontSize: '16px', color: '#08c' }} />} disabled="true">
                </Menu.Item>
                <Menu.Item key="news" icon={<ReadOutlined />}>
                    <Link to="/admin/news">News</Link>
                </Menu.Item>
                <Menu.Item key="books" icon={<BookOutlined />}>
                    <Link to="/admin/books">Books</Link>
                </Menu.Item>
            </Menu>
            <div className='header_tail'>
                <Dropdown overlay={menuUser} placement="bottomLeft">
                    <div className="userSelector">
                        <p>{props.username}</p>
                        <Avatar src="https://joeschmoe.io/api/v1/random" />
                    </div>
                </Dropdown>
                <Dropdown overlay={menuLanguage} placement="bottomRight">
                    <GlobalOutlined style={{ fontSize: '24px', color: '#08c' }} />
                </Dropdown>
            </div>

        </div>
    );
};
export default AdminHeader;