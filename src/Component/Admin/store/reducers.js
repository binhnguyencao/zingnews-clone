import { INIT_STATE } from "./state";

import { SAVE_ALL_BOOKS, SAVE_ALL_NEWS, SET_LOADING_STEP } from "./constants";

import produce from "immer"

export default function dataReducer(state = INIT_STATE, action) {
    return produce(state, (draft) => {
        switch (action.type) {
            case SET_LOADING_STEP:
                draft.isLoading = action.payload;
                break;
            case SAVE_ALL_NEWS:
                draft.news = action.payload;
                break;
            case SAVE_ALL_BOOKS:
                draft.books = action.payload;
                break;
            default:
                return state;
        }
    });
}