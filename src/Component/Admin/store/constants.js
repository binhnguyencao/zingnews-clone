const ADMIN = "adminpage"

const SET_LOADING_STEP = `${ADMIN}/setLoadingAdmin`

//news

const GET_ALL_NEWS = `${ADMIN}/getAllNews`

const SAVE_ALL_NEWS = `${ADMIN}/saveAllNews`

const CREATE_NEWS = `${ADMIN}/createNews`

const GET_DETAIL_NEWS = `${ADMIN}/getDetailNews`



const UPDATE_NEWS = `${ADMIN}/updateNews`

const DELETE_NEWS = `${ADMIN}/deleteNews`

//books

const GET_ALL_BOOKS = `${ADMIN}/getAllBooks`

const SAVE_ALL_BOOKS = `${ADMIN}/saveAllBooks`

const CREATE_BOOKS = `${ADMIN}/createBooks`

const GET_DETAIL_BOOKS = `${ADMIN}/getDetailBooks`

const UPDATE_BOOKS = `${ADMIN}/updateBooks`

const DELETE_BOOKS = `${ADMIN}/deleteBooks`





export {
    ADMIN,
    SET_LOADING_STEP,
    //news
    GET_ALL_NEWS,
    SAVE_ALL_NEWS,
    CREATE_NEWS,
    GET_DETAIL_NEWS,
    UPDATE_NEWS,
    DELETE_NEWS,
    //books
    GET_ALL_BOOKS,
    SAVE_ALL_BOOKS,
    CREATE_BOOKS,
    GET_DETAIL_BOOKS,
    UPDATE_BOOKS,
    DELETE_BOOKS


}