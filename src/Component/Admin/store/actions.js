import { act } from 'react-dom/test-utils';
import * as cts from './constants';


export function setLoadingStep(payload) {
    return {
        type: cts.SET_LOADING_STEP,
        payload
    }
}

//news

export function getAllNewsAction(payload) {
    return {
        type: cts.GET_ALL_NEWS,
        payload
    }
}

export function saveAllNewsAction(payload) {
    return {
        type: cts.SAVE_ALL_NEWS,
        payload
    }
}

export const createNewsAction = (dispatch) => (payload) =>
    new Promise((resolve) =>
        dispatch({ type: cts.CREATE_NEWS, payload, resolve })
    );

export const getDetailNewsAction = (dispatch) => (payload) =>
    new Promise((resolve) =>
        dispatch({ type: cts.GET_DETAIL_NEWS, payload, resolve })
    );

export const updateNewsAction = (dispatch) => (payload) =>
    new Promise((resolve) =>
        dispatch({ type: cts.UPDATE_NEWS, payload, resolve })
    );

export const deleteNewsAction = (dispatch) => (payload) =>
    new Promise((resolve) =>
        dispatch({ type: cts.DELETE_NEWS, payload, resolve })
    );




//books

export function getAllBooksAction(payload) {
    return {
        type: cts.GET_ALL_BOOKS,
        payload
    }
}

export function saveAllBooksAction(payload) {
    return {
        type: cts.SAVE_ALL_BOOKS,
        payload
    }
}

export const createBooksAction = (dispatch) => (payload) =>
    new Promise((resolve) =>
        dispatch({ type: cts.CREATE_BOOKS, payload, resolve })
    );

export const getDetailBooksAction = (dispatch) => (payload) =>
    new Promise((resolve) =>
        dispatch({ type: cts.GET_DETAIL_BOOKS, payload, resolve })
    );

export const updateBooksAction = (dispatch) => (payload) =>
    new Promise((resolve) =>
        dispatch({ type: cts.UPDATE_BOOKS, payload, resolve })
    );

export const deleteBooksAction = (dispatch) => (payload) =>
    new Promise((resolve) =>
        dispatch({ type: cts.DELETE_BOOKS, payload, resolve })
    );












