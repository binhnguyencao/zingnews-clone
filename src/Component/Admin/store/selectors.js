import { INIT_STATE } from "./state";

import { createSelector } from "reselect";

const selectNews = (state) => state.dataReducer || INIT_STATE

const selectLoading = createSelector(selectNews, (state) => state.isLoading)

const selectAllNews = createSelector(selectNews, (state) => state.news || [])

const selectAllBooks = createSelector(selectNews, (state) => state.books || [])

// const selectNewStudent = createSelector(selectMyClass, (state) => state.newStudent || [])

// const selectCurrentStudent = createSelector(selectMyClass, (state) => state.currentStudent || [])



export {
    selectAllNews,
    selectLoading,
    selectAllBooks

}