import { takeLatest, put, call } from 'redux-saga/effects'

import { saveAllNewsAction, setLoadingStep, saveAllBooksAction } from './actions'

import { GET_ALL_NEWS, CREATE_NEWS, GET_DETAIL_NEWS, UPDATE_NEWS, DELETE_NEWS } from './constants'

import { GET_ALL_BOOKS, CREATE_BOOKS, GET_DETAIL_BOOKS, UPDATE_BOOKS, DELETE_BOOKS } from './constants'

import { getListNewsService, createNewsService, getDetailNewsService, updateNewsService, deleteNewsService } from '../api/api'

import { getListBooksService, createBooksService, getDetailBooksService, updateBooksService, deleteBooksService, } from '../api/apiBooks'


function* fetchNewsSaga() {
    yield put(setLoadingStep(true))
    try {
        const response = yield call(getListNewsService)
        // const copy = response.data.slice()
        // yield put(saveAllNewsAction(copy.reverse()))
        yield put(saveAllNewsAction(response.data))
        console.log(response)
        yield put(setLoadingStep(false))
    }
    catch (error) {
        yield put(setLoadingStep(false))
    }
}

function* createNewsSaga({ payload, resolve }) {
    yield put(setLoadingStep(true))
    try {
        const response = yield call(createNewsService, payload)
        //console.log(response)
        yield put(setLoadingStep(false))
        resolve(response)
    }
    catch (error) {
        resolve(null)
        yield put(setLoadingStep(false))
    }
}

function* getDetailNewsSaga({ payload, resolve }) {
    yield put(setLoadingStep(true))
    try {
        const response = yield call(getDetailNewsService, payload)
        resolve(response)
        //console.log(response)
        yield put(setLoadingStep(false))
    }
    catch (error) {
        resolve(null)
        yield put(setLoadingStep(false))
    }
}

function* updateNewsSaga({ payload, resolve }) {
    yield put(setLoadingStep(true))
    try {
        //console.log(payload)
        const response = yield call(updateNewsService, payload)
        resolve(response)
        yield put(setLoadingStep(false))
    }
    catch (error) {
        resolve(null)
        yield put(setLoadingStep(false))
    }
}


function* deleteNewsSaga({ payload, resolve }) {
    console.log(payload)
    yield put(setLoadingStep(true))
    try {
        const response = yield call(deleteNewsService, payload)
        //console.log(response)
        yield put(setLoadingStep(false))
        resolve(response.status)
    }
    catch (error) {
        resolve(null)
        yield put(setLoadingStep(false))
    }
}


//books



function* fetchBooksSaga() {
    yield put(setLoadingStep(true))
    try {
        const response = yield call(getListBooksService)
        const copy = response.data.slice()
        yield put(saveAllBooksAction(copy.reverse()))

        //console.log(response)
        yield put(setLoadingStep(false))
    }
    catch (error) {
        yield put(setLoadingStep(false))
    }
}

function* createBooksSaga({ payload, resolve }) {
    yield put(setLoadingStep(true))
    try {
        const response = yield call(createBooksService, payload)
        //console.log(response)
        yield put(setLoadingStep(false))
        resolve(response)
    }
    catch (error) {
        resolve(null)
        yield put(setLoadingStep(false))
    }
}

function* getDetailBooksSaga({ payload, resolve }) {
    yield put(setLoadingStep(true))
    try {
        const response = yield call(getDetailBooksService, payload)
        resolve(response)
        //console.log(response)
        yield put(setLoadingStep(false))
    }
    catch (error) {
        resolve(null)
        yield put(setLoadingStep(false))
    }
}

function* updateBooksSaga({ payload, resolve }) {
    yield put(setLoadingStep(true))
    try {
        //console.log(payload)
        const response = yield call(updateBooksService, payload)
        resolve(response)
        yield put(setLoadingStep(false))
    }
    catch (error) {
        resolve(null)
        yield put(setLoadingStep(false))
    }
}

function* deleteBooksSaga({ payload, resolve }) {
    console.log(payload)
    yield put(setLoadingStep(true))
    try {
        const response = yield call(deleteBooksService, payload)
        //console.log(response)
        yield put(setLoadingStep(false))
        resolve(response.status)
    }
    catch (error) {
        resolve(null)
        yield put(setLoadingStep(false))
    }
}



export function* sagaNews() {
    //news
    yield takeLatest(GET_ALL_NEWS, fetchNewsSaga)
    yield takeLatest(CREATE_NEWS, createNewsSaga)
    yield takeLatest(GET_DETAIL_NEWS, getDetailNewsSaga)
    yield takeLatest(UPDATE_NEWS, updateNewsSaga)
    yield takeLatest(DELETE_NEWS, deleteNewsSaga)

    //books
    yield takeLatest(GET_ALL_BOOKS, fetchBooksSaga)
    yield takeLatest(CREATE_BOOKS, createBooksSaga)
    yield takeLatest(GET_DETAIL_BOOKS, getDetailBooksSaga)
    yield takeLatest(UPDATE_BOOKS, updateBooksSaga)
    yield takeLatest(DELETE_BOOKS, deleteBooksSaga)


}