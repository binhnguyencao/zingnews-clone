import axios from "axios";

const URL = "https://62fd0400b9e38585cd4b1834.mockapi.io"

const getListBooksService = () => axios.get(`${URL}/books`)

const createBooksService = (data) => axios.post(`${URL}/books`, data)

const getDetailBooksService = (id) => axios.get(`${URL}/books/${id}`)

const updateBooksService = ({ id, data }) => axios.put(`${URL}/books/${id}`, data)

const deleteBooksService = (id) => axios.delete(`${URL}/books/${id}`)

export {
    getListBooksService,
    createBooksService,
    getDetailBooksService,
    updateBooksService,
    deleteBooksService
}

