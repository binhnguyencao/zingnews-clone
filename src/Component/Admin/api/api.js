import axios from "axios";

const URL = "https://62fd0400b9e38585cd4b1834.mockapi.io"

const getListNewsService = () => axios.get(`${URL}/news`)

const createNewsService = (data) => axios.post(`${URL}/news`, data)

const getDetailNewsService = (id) => axios.get(`${URL}/news/${id}`)

const updateNewsService = ({ id, data }) => axios.put(`${URL}/news/${id}`, data)

const deleteNewsService = (id) => axios.delete(`${URL}/news/${id}`)

export {
    getListNewsService,
    createNewsService,
    getDetailNewsService,
    updateNewsService,
    deleteNewsService
}