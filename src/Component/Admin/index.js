import { useEffect, useState } from "react";
import { Route, Routes } from "react-router-dom";
import AdminHeader from "./header";
import Cookies from "js-cookie";
import routersAdmin from "./routersAdmin";
import { useNavigate } from "react-router-dom";

const Admin = () => {
  const navigate = useNavigate();
  const showMenu = (routersAdmin) => {
    let result = "";
    if (routersAdmin) {
      result = routersAdmin.map((item, index) => {
        return <Route key={index} path={item.path} element={item.component()}></Route>;
      });
    }
    return result;
  };

  // useEffect(() => {
  //     const user = Cookies.get('username');
  //     if (!user) {
  //         navigate("/login");
  //     }
  // }, [])
  return (
    <div>
      <AdminHeader username={Cookies.get("username")} />
      <Routes>{showMenu(routersAdmin)}</Routes>
    </div>
  );
};
export default Admin;
