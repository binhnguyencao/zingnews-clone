import React from "react"
import XuatBan from '../Pages/XuatBan/index'
import KinhDoanh from "../Pages/KinhDoanh/index"
import TrangChu from "../Pages/TrangChu"
import NewsDetail from "../Pages/NewsDetail"


import Sach from "../Pages/Sach/index"
import SachDetail from "../Pages/SachDetail"

const routers = [
    {
        path: "/xuat-ban",
        component: () => <XuatBan />,
    },
    {
        path: "/kinh-doanh",
        component: () => <KinhDoanh />,
    },
    {
        path: "/*",
        component: () => <TrangChu />,
    },
    {
        path: "/page/:id",
        component: () => <NewsDetail />,
    },
    {
        path: "/sach",
        component: () => <Sach />,
    },
    {
        path: "/sach/:id",
        component: () => <SachDetail />,
    },


]
export default routers