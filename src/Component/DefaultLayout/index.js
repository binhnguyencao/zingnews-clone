import { useEffect, useState } from "react";
import { Route, Routes } from "react-router-dom";
import Header from "../Header/index_header";
import Cookies from "js-cookie";
import routers from "../routers";
import { useNavigate } from "react-router-dom";
import TrangChu from "../../Pages/TrangChu";

const DefaultLayout = () => {
  const navigate = useNavigate();
  const showMenu = (routers) => {
    let result = "";
    if (routers) {
      result = routers.map((item, index) => {
        return <Route key={index} path={item.path} element={item.component()}></Route>;
      });
    }
    return result;
  };

  // useEffect(() => {
  //     const user = Cookies.get('username');
  //     if (!user) {
  //         navigate("/login");
  //     }
  // }, [])
  return (
    <div>
      <Header username={Cookies.get("username")} />
      <hr className="hrHeader" />
      {<Routes>{showMenu(routers)}</Routes>}
    </div>
  );
};
export default DefaultLayout;
