
import './App.css';
import { Routes, Route, BrowserRouter } from 'react-router-dom'
import Login from './Component/Login';
import DefaultLayout from './Component/DefaultLayout';
import Admin from './Component/Admin/index';
function App() {

  return (
    <div className="App">

      <BrowserRouter>
        <Routes>
          <Route path="/login" element={<Login />} />
          <Route path="/admin/*" element={<Admin />} />
          <Route path="/*" element={<DefaultLayout />} />
        </Routes>
      </BrowserRouter>




    </div>
  );
}

export default App;
