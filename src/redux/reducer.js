import { combineReducers } from "redux";
import studentReducer from "../Pages/XuatBan/store/reducers";
import loginReducer from "../Component/Login/store/reducers";
import dataReducer from "../Component/Admin/store/reducers";
export default function createReducer() {
    const rootReducer = combineReducers({
        studentReducer,
        loginReducer,
        dataReducer

    })
    return rootReducer
}