import { all } from "redux-saga/effects"

import { sagaStudent } from "../Pages/XuatBan/store/sagas"
import { sagaLogin } from "../Component/Login/store/sagas"
import { sagaNews } from "../Component/Admin/store/sagas"

// eslint-disable-next-line import/no-anonymous-default-export
export default function* () {
    yield all([
        //student
        sagaStudent(),
        //login
        sagaLogin(),
        //news
        sagaNews()
    ])
}