import './index.scss'
import { Col, Row } from 'antd';
import { useEffect, useState } from "react"

import { createStructuredSelector } from 'reselect'

import { getAllBooksAction } from "../../Component/Admin/store/actions";
import { selectAllBooks } from "../../Component/Admin/store/selectors";
import { connect } from "react-redux"
import { compose, mapProps } from "recompose";

import BooksComponent from '../../Component/BooksComponent';
const Sach = (props) => {

    const { getAllBooksDispatch } = props
    const { listBooks } = props

    useEffect(() => {
        getAllBooksDispatch()

        console.log(props)
    }, [])

    const arrType1 = listBooks.filter(item => item.type === 1 && item.showing === true)
    const arrType2 = listBooks.filter(item => item.type === 2 && item.showing === true)
    const arrType3 = listBooks.filter(item => item.type === 3 && item.showing === true)

    return (
        <div className="main2">
            <div className="articleDetails">
                <div className="titleDetail">
                    <h1>Sách Hay</h1>
                </div>
            </div>
            <hr className='hrSachHay'></hr>
            <Row
                gutter={{
                    xs: 8,
                    sm: 16,
                    md: 24,
                    lg: 32,
                }}
            >
                <Col className="gutter-row" span={16}>
                    {arrType2.map(item => <BooksComponent id={item.id} type={item.type} alt={item.src} src={item.image} title={item.title} description={item.description} />)}
                </Col>
                <Col className="gutter-row" span={8}>
                    {arrType3.map(item => <BooksComponent id={item.id} type={item.type} alt={item.src} src={item.image} title={item.title} description={item.description} />)}
                </Col>
            </Row>
            <div className='type1List'>
                {arrType1.map(item => <BooksComponent id={item.id} type={item.type} alt={item.src} src={item.image} title={item.title} description={item.description} />)}
            </div>
        </div>
    )
}



const mapStateToProps = createStructuredSelector({
    listBooks: selectAllBooks,

})
const mapDispatchtoProps = (dispatch) => ({
    getAllBooksDispatch: (payload) => dispatch(getAllBooksAction(payload)),

})

const withConnect = connect(mapStateToProps, mapDispatchtoProps)
export default compose(withConnect)(Sach);


