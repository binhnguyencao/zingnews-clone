import './index.scss'
import { Col, Row } from 'antd';
import { useEffect, useState } from "react"

import { createStructuredSelector } from 'reselect'

import { getAllNewsAction } from "../../Component/Admin/store/actions";
import { selectAllNews } from "../../Component/Admin/store/selectors";
import { connect } from "react-redux"
import { compose, mapProps } from "recompose";

import NewsComponent from '../../Component/NewsComponent';
const TrangChu = (props) => {

    const { getAllNewsDispatch } = props
    const { listNews } = props



    useEffect(() => {
        getAllNewsDispatch()

    }, [])

    const arrType1 = listNews.filter(item => item.type === 1 && item.showing === true).slice(0, 5)
    const arrType2 = listNews.filter(item => item.type === 2 && item.showing === true).slice(0, 1)
    const arrType3 = listNews.filter(item => item.type === 3 && item.showing === true).slice(0, 2)

    return (
        <div className="main">
            <div className="hagTag">
                <div>
                    <img src='https://static-znews.zingcdn.me/images/stat.svg' alt='icon'></img>
                    <button type="button" className="btn">#Dịch sốt xuất huyết</button>
                    <button type="button" className="btn">#U23 Việt Nam</button>
                    <button type="button" className="btn">#Dấu ấn bản lĩnh</button>
                    <button type="button" className="btn">#Johnny Depp và Amber Heard</button>
                </div>
                <div >
                    <b>TP.Hồ Chí Minh</b> 33<sup>o</sup>C/ 25-33<sup>o</sup>C
                    <img src='https://static-znews.zingcdn.me/images/icons/weather/v2/hazy.png' alt='icon' height={"30px"} width={"auto"}></img>
                </div>
            </div>
            <Row

                style={{ marginTop: "10px" }}
            >
                <Col className="gutter-row" xl={{ span: 7, order: 1 }} lg={{ span: 9, order: 2 }} md={{ span: 24, order: 2 }} sm={{ span: 24, order: 1 }}>
                    {arrType1.map(item => <NewsComponent id={item.id} type={item.type} alt={item.src} src={item.image} title={item.title} description={item.description} />)}
                </Col>
                <Col className="gutter-row" xl={{ span: 17, order: 2 }} lg={{ span: 15, order: 1 }} md={{ span: 24, order: 1 }} sm={{ span: 24, order: 1 }}>
                    <Row
                        gutter={{
                            xs: 8,
                            sm: 16,
                            md: 24,
                            lg: 32,
                        }}
                    >
                        <Col className="collumType2" xl={{ span: 16 }} lg={{ span: 24 }}  >
                            {arrType2.map(item => <NewsComponent id={item.id} type={item.type} alt={item.src} src={item.image} title={item.title} description={item.description} />)}
                        </Col>
                        <Col className="collumType3" xl={{ span: 8 }} lg={{ span: 24 }}>
                            {arrType3.map(item => <NewsComponent id={item.id} type={item.type} alt={item.src} src={item.image} title={item.title} description={item.description} />)}
                        </Col>
                    </Row>
                </Col>
            </Row>
        </div>
    )
}




const mapStateToProps = createStructuredSelector({
    listNews: selectAllNews,

})
const mapDispatchtoProps = (dispatch) => ({
    getAllNewsDispatch: (payload) => dispatch(getAllNewsAction(payload)),
})

const withConnect = connect(mapStateToProps, mapDispatchtoProps)
export default compose(withConnect)(TrangChu);
