import React, { useEffect, useState } from 'react'
import './style.scss'
import { useParams } from "react-router-dom"
import { createStructuredSelector } from 'reselect'



import { getDetailNewsAction } from "../../Component/Admin/store/actions";
import { connect } from "react-redux"
import { compose, mapProps } from "recompose";


const NewsDetail = (props) => {
    let { id } = useParams()
    const { getDetailNewsDispatch } = props
    const [news, setNews] = useState()
    useEffect(() => {

        const callData = async () => {
            const response = await getDetailNewsDispatch(id);
            console.log(response)
            if (response.status === 200) {
                setNews(response.data)
            }

        }
        callData()

    }, [])
    return (
        <>
            <div className="articleDetails">
                <div className="titleDetail">
                    <h1>{news?.title}</h1>
                </div>
                <div className="titleHeighlight">
                    <b>{news?.description}</b>
                </div>
                <div className="contentDetail">
                    {news?.content}
                </div>
                <div className="pictureDetail">
                    <div className="imageDetail" style={{ backgroundImage: `url(${news?.image})` }}></div>
                </div>
            </div>
        </>
    )
}


const mapStateToProps = createStructuredSelector({

})
const mapDispatchtoProps = (dispatch) => ({
    getDetailNewsDispatch: (payload) => getDetailNewsAction(dispatch)(payload),
})

const withConnect = connect(mapStateToProps, mapDispatchtoProps)
export default compose(withConnect)(NewsDetail);
