import { takeLatest, put, call } from 'redux-saga/effects'

import { saveAllStudentAction, setLoadingStep, saveNewStudentAction, saveCurrentStudentAction } from './actions'

import { GET_ALL_STUDENT, CREATE_STUDENT, GET_CURRENT_STUDENT, UPDATE_CURRENT_STUDENT, DELETE_CURRENT_STUDENT } from './constants'

import { getListStudentService, createStudentService, getDetailStudentService, updateStudentService, deleteCurrentStudentService } from '../../../api'


function* fetchStudentSaga() {
    yield put(setLoadingStep(true))
    try {
        const response = yield call(getListStudentService)
        yield put(saveAllStudentAction(response.data))
        console.log(response)
        yield put(setLoadingStep(false))
    }
    catch (error) {
        yield put(setLoadingStep(false))
    }
}

function* createStudentSaga({ payload, resolve }) {
    yield put(setLoadingStep(true))
    try {
        const response = yield call(createStudentService, payload)
        console.log(response)
        yield put(saveNewStudentAction(response.data))
        yield put(setLoadingStep(false))
        resolve(response)
    }
    catch (error) {
        resolve(null)
        yield put(setLoadingStep(false))
    }
}

function* getCurrentStudentSaga({ payload, resolve }) {
    yield put(setLoadingStep(true))
    try {
        const response = yield call(getDetailStudentService, payload)
        resolve(response)
        console.log(response)
        yield put(saveCurrentStudentAction(response.data))
        yield put(setLoadingStep(false))

    }
    catch (error) {
        resolve(null)
        yield put(setLoadingStep(false))
    }
}

function* updateCurrentStudentSaga({ payload, resolve }) {
    yield put(setLoadingStep(true))
    try {
        console.log(payload)
        const response = yield call(updateStudentService, payload)
        resolve(response)
        yield put(setLoadingStep(false))

    }
    catch (error) {
        // resolve(null)
        yield put(setLoadingStep(false))
    }
}


function* deleteCurrentStudentSaga({ payload, resolve }) {
    console.log(payload)
    yield put(setLoadingStep(true))
    try {
        const response = yield call(deleteCurrentStudentService, payload)
        console.log(response)
        yield put(setLoadingStep(false))
        resolve(response.status)
    }
    catch (error) {
        resolve(null)
        yield put(setLoadingStep(false))
    }
}

export function* sagaStudent() {
    yield takeLatest(GET_ALL_STUDENT, fetchStudentSaga)
    yield takeLatest(CREATE_STUDENT, createStudentSaga)
    yield takeLatest(GET_CURRENT_STUDENT, getCurrentStudentSaga)
    yield takeLatest(UPDATE_CURRENT_STUDENT, updateCurrentStudentSaga)
    yield takeLatest(DELETE_CURRENT_STUDENT, deleteCurrentStudentSaga)
}