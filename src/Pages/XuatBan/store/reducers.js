import { INIT_STATE } from "./state";

import { SAVE_ALL_STUDENT, SET_LOADING_STEP, SAVE_NEW_STUDENT, SAVE_CURRENT_STUDENT } from "./constants";

import produce from "immer"

export default function studentReducer(state = INIT_STATE, action) {
    return produce(state, (draft) => {
        switch (action.type) {
            case SET_LOADING_STEP:
                draft.isLoading = action.payload;
                break;
            case SAVE_ALL_STUDENT:
                draft.students.data = action.payload;
                break;
            case SAVE_NEW_STUDENT:
                draft.newStudent = action.payload;
                break;
            case SAVE_CURRENT_STUDENT:
                draft.currentStudent = action.payload;
                break;
            default:
                return state;
        }
    });
}