export const INIT_STATE = {
    isLoading: false,
    students: {
        data: []
    },
    newStudent: {},
    currentStudent: {}

}