import { INIT_STATE } from "./state";

import { createSelector } from "reselect";

const selectMyClass = (state) => state.studentReducer || INIT_STATE

const selectLoading = createSelector(selectMyClass, (state) => state.isLoading)

const selectStudents = createSelector(selectMyClass, (state) => state.students?.data || [])

const selectNewStudent = createSelector(selectMyClass, (state) => state.newStudent || [])

const selectCurrentStudent = createSelector(selectMyClass, (state) => state.currentStudent || [])



export {
    selectStudents,
    selectLoading,
    selectNewStudent,
    selectCurrentStudent
}