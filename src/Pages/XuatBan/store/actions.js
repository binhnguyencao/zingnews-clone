import {
    GET_ALL_STUDENT, SAVE_ALL_STUDENT,

    SET_LOADING_STEP,

    CREATE_STUDENT, SAVE_NEW_STUDENT,

    GET_CURRENT_STUDENT, SAVE_CURRENT_STUDENT, UPDATE_CURRENT_STUDENT,
    DELETE_CURRENT_STUDENT
} from './constants';


export function setLoadingStep(payload) {
    return {
        type: SET_LOADING_STEP,
        payload
    }
}

export function getAllStudentAction(payload) {
    return {
        type: GET_ALL_STUDENT,
        payload
    }
}

export function saveAllStudentAction(payload) {
    return {
        type: SAVE_ALL_STUDENT,
        payload
    }
}

export function createStudentAction(payload) {
    return {
        type: CREATE_STUDENT,
        payload
    }
}


export function saveNewStudentAction(payload) {
    return {
        type: SAVE_NEW_STUDENT,
        payload
    }
}

export const asyncCreateStudentAction = (dispatch) => (payload) =>
    new Promise((resolve) =>
        dispatch({ type: CREATE_STUDENT, payload, resolve })
    );


export const getCurrentStudentAction = (dispatch) => (payload) =>
    new Promise((resolve) =>
        dispatch({ type: GET_CURRENT_STUDENT, payload, resolve })
    );


export function saveCurrentStudentAction(payload) {
    return {
        type: SAVE_CURRENT_STUDENT,
        payload
    }
}

export const updateCurrentStudentAction = (dispatch) => (payload) =>
    new Promise((resolve) =>
        dispatch({ type: UPDATE_CURRENT_STUDENT, payload, resolve })
    );



export const deleteCurrentStudentAction = (dispatch) => (payload) =>
    new Promise((resolve) =>
        dispatch({ type: DELETE_CURRENT_STUDENT, payload, resolve })
    );
