const MY_CLASS = "myclass"

const SET_LOADING_STEP = `${MY_CLASS}/setGlobalLoading`

const GET_ALL_STUDENT = `${MY_CLASS}/getAllStudent`

const SAVE_ALL_STUDENT = `${MY_CLASS}/saveAllStudent`

const CREATE_STUDENT = `${MY_CLASS}/createStudent`

const SAVE_NEW_STUDENT = `${MY_CLASS}/saveNewStudent`

const GET_CURRENT_STUDENT = `${MY_CLASS}/getCurrentStudent`

const SAVE_CURRENT_STUDENT = `${MY_CLASS}/saveCurrentStudent`

const UPDATE_CURRENT_STUDENT = `${MY_CLASS}/updateCurrentStudent`

const DELETE_CURRENT_STUDENT = `${MY_CLASS}/deleteCurrentStudent`


export {
    MY_CLASS,
    GET_ALL_STUDENT,
    SAVE_ALL_STUDENT,
    SET_LOADING_STEP,
    CREATE_STUDENT,
    SAVE_NEW_STUDENT,
    GET_CURRENT_STUDENT,
    SAVE_CURRENT_STUDENT,
    UPDATE_CURRENT_STUDENT,
    DELETE_CURRENT_STUDENT
}