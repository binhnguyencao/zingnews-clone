import { Table, Space, Modal, Form, Input, InputNumber, Button, notification } from "antd"
import { SmileOutlined } from '@ant-design/icons';

import { useEffect, useState } from "react"

import { createStructuredSelector } from 'reselect'


import { selectStudents, selectLoading, selectNewStudent, selectCurrentStudent } from "./store/selectors";
import { getAllStudentAction, createStudentAction, asyncCreateStudentAction, getCurrentStudentAction, updateCurrentStudentAction, deleteCurrentStudentAction } from "./store/actions";
import { connect } from "react-redux"
import { compose, mapProps } from "recompose";


const XuatBan = (props) => {


    const [form] = Form.useForm()

    const [isShowKey, seIsSHowKey] = useState(true);


    const { getAllStudentDispatch, createStudentDispatch, getCurrentStudentDispatch, updateCurrentStudentDispatch, deleteCurrentStudentDispatch } = props
    const { listStudent, isLoading, newStudent, currentStudent } = props



    useEffect(() => {
        getAllStudentDispatch()
        console.log(props)
    }, [])


    const onDelete = async (id) => {
        const response = await deleteCurrentStudentDispatch(id);
        if (response === 200) {
            getAllStudentDispatch()
        }
    }

    const onEdit = async (id) => {
        seIsSHowKey(true)
        const response = await getCurrentStudentDispatch(id);
        if (response.status === 200) {
            form.setFieldsValue({
                name: response.data.name,
                age: response.data.age,
                key: response.data.key,
                email: response.data.email,
                phone: response.data.phone,
                id: response.data.id,
            })
            setIsModalVisible(true);
        }
    }

    const [isModalVisible, setIsModalVisible] = useState(false);


    const handleOk = () => {
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);

    };
    const onSubmit = async (values) => {
        if (isShowKey) {
            const response = await updateCurrentStudentDispatch({ id: values.id, data: values })
            if (response.status === 200) {
                getAllStudentDispatch()
            }

            form.resetFields();
            handleCancel();
        }
        else {
            const response = await createStudentDispatch(values)
            if (response.status === 201) {
                getAllStudentDispatch()
            }
            const table = <table>
                <tr>
                    <th>Name</th>
                    <th>{response.data.name}</th>
                </tr>
                <tr>
                    <th>Age</th>
                    <th>{response.data.age}</th>
                </tr>
                <tr>
                    <th>Email</th>
                    <th>{response.data.email}</th>
                </tr>
                <tr>
                    <th>Phone</th>
                    <th>{response.data.phone}</th>
                </tr>
            </table>
            notification.success({
                message: 'Add student success',
                description: table,
            });


            form.resetFields();
            handleCancel();
        }

    }
    const columns = [
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
            render: (text) => <a href={text}>{text}</a>,
        },
        {
            title: 'Age',
            dataIndex: 'age',
            key: 'age',
        },
        {
            title: 'Phone',
            dataIndex: 'phone',
            key: 'phone',
        },
        {
            title: 'Email',
            dataIndex: 'email',
            key: 'email',
        },
        {
            title: 'Action',
            key: 'action',
            render: (record) => (
                <Space size="middle">
                    <button onClick={() => onDelete(record.id)}>Delete</button>
                    <button onClick={() => onEdit(record.id)}>Edit</button>
                </Space>
            ),
        },
    ]
    const createStudent = () => {
        console.log(props)
        setIsModalVisible(true);
        seIsSHowKey(false)
    };
    return (
        <div>
            <Button onClick={() => createStudent()}>Create Student</Button>
            <Table loading={isLoading} columns={columns} dataSource={listStudent} />
            <Modal title="Basic Modal" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel} footer={
                <Space>
                    <Button onClick={handleCancel}>Cancel</Button>
                    <Button type='primary' htmlType='submit' form='form'>OK</Button>
                </Space>
            }>
                <Form
                    form={form}
                    name="form"
                    wrapperCol={{
                        span: 24,
                    }}
                    initialValues={{
                        remember: true,
                    }}
                    onFinish={onSubmit}
                    autoComplete="off"
                >
                    <Form.Item label="key" name="key">
                        <Input disabled={isShowKey} />
                    </Form.Item>
                    <Form.Item label="ID" name="id">
                        <Input />
                    </Form.Item>

                    <Form.Item
                        label="Name"
                        name="name"
                        rules={[
                            {
                                required: true,
                                message: "Please input your name!",
                            },
                        ]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        label="Age"
                        name="age"
                        rules={[
                            {
                                required: true,
                                message: "Please input your age!",
                            },
                        ]}
                    >
                        <InputNumber style={{ width: "100%" }} />
                    </Form.Item>

                    <Form.Item
                        label="Phone"
                        name="phone"
                        rules={[
                            {
                                required: true,
                                message: "Please input your phone!",
                            },
                        ]}
                    >
                        <InputNumber style={{ width: "100%" }} />
                    </Form.Item>

                    <Form.Item
                        label="Email"
                        name="email"
                        rules={[
                            {
                                required: true,
                                message: "Please input your email!",
                            }
                        ]}
                    >
                        <Input />
                    </Form.Item>
                </Form>
            </Modal>
        </div>
    )
};
const mapStateToProps = createStructuredSelector({
    listStudent: selectStudents,
    isLoading: selectLoading,
    newStudent: selectNewStudent,
    currentStudent: selectCurrentStudent

})
const mapDispatchtoProps = (dispatch) => ({
    getAllStudentDispatch: (payload) => dispatch(getAllStudentAction(payload)),
    //createStudentDispatch: (payload) => dispatch(createStudentAction(payload)),
    createStudentDispatch: (payload) => asyncCreateStudentAction(dispatch)(payload),
    getCurrentStudentDispatch: (payload) => getCurrentStudentAction(dispatch)(payload),
    updateCurrentStudentDispatch: (payload) => updateCurrentStudentAction(dispatch)(payload),
    deleteCurrentStudentDispatch: (payload) => deleteCurrentStudentAction(dispatch)(payload),
})

const withConnect = connect(mapStateToProps, mapDispatchtoProps)
export default compose(withConnect)(XuatBan);