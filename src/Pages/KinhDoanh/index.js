import { Table, Space, Modal, Form, Input, InputNumber, Button } from "antd"
import { useEffect, useState } from "react"

const KinhDoanh = () => {

    const [isLoading, seIsLoading] = useState(false);
    const [form] = Form.useForm()
    const [list, setList] = useState()
    const [isShowKey, seIsSHowKey] = useState(true);

    const setDataa = () => {

    }

    useEffect(() => {
        setDataa()
    }, [])
    const onDelete = (key) => {
        seIsLoading(true)

    }

    const onEdit = (id) => {
        seIsLoading(true)
        setIsModalVisible(true);

    }

    const [isModalVisible, setIsModalVisible] = useState(false);


    const handleOk = () => {
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
        seIsLoading(false)
    };

    const onSubmit = (values) => {
        seIsLoading(true)

        form.resetFields()
        handleCancel()
    }
    const columns = [
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
            render: (text) => <a href={text}>{text}</a>,
        },
        {
            title: 'Age',
            dataIndex: 'age',
            key: 'age',
        },
        {
            title: 'Phone',
            dataIndex: 'phone',
            key: 'phone',
        },
        {
            title: 'Email',
            dataIndex: 'email',
            key: 'email',
        },
        {
            title: 'Action',
            key: 'action',
            render: (record) => (
                <Space size="middle">
                    <button onClick={() => onDelete(record.id)}>Delete</button>
                    <button onClick={() => onEdit(record.id)}>Edit</button>
                </Space>
            ),
        },
    ]
    const createStudent = () => {
        setIsModalVisible(true);
        seIsSHowKey(false)
    };
    return (
        <div>
            <Button onClick={() => createStudent()}>Create Student</Button>
            <Table loading={isLoading} columns={columns} dataSource={list} />
            <Modal title="Basic Modal" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel} footer={
                <Space>
                    <Button onClick={handleCancel}>Cancel</Button>
                    <Button type='primary' htmlType='submit' form='form'>OK</Button>
                </Space>
            }>
                <Form
                    form={form}
                    name="form"
                    wrapperCol={{
                        span: 24,
                    }}
                    initialValues={{
                        remember: true,
                    }}
                    onFinish={onSubmit}
                    autoComplete="off"
                >
                    <Form.Item label="key" name="key">
                        <Input disabled={isShowKey} />
                    </Form.Item>
                    <Form.Item label="ID" name="id">
                        <Input />
                    </Form.Item>

                    <Form.Item
                        label="Name"
                        name="name"
                        rules={[
                            {
                                required: true,
                                message: "Please input your name!",
                            },
                        ]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        label="Age"
                        name="age"
                        rules={[
                            {
                                required: true,
                                message: "Please input your age!",
                            },
                        ]}
                    >
                        <InputNumber style={{ width: "100%" }} />
                    </Form.Item>

                    <Form.Item
                        label="Phone"
                        name="phone"
                        rules={[
                            {
                                required: true,
                                message: "Please input your phone!",
                            },
                        ]}
                    >
                        <InputNumber style={{ width: "100%" }} />
                    </Form.Item>

                    <Form.Item
                        label="Email"
                        name="email"
                        rules={[
                            {
                                required: true,
                                message: "Please input your email!",
                            }
                        ]}
                    >
                        <Input />
                    </Form.Item>
                </Form>
            </Modal>
        </div>
    )
};

export default KinhDoanh