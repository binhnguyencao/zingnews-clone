import React, { useEffect, useState } from 'react'
import './style.scss'
import { useParams } from "react-router-dom"
import { createStructuredSelector } from 'reselect'


import { getDetailBooksAction } from "../../Component/Admin/store/actions";
import { connect } from "react-redux"
import { compose, mapProps } from "recompose";

const BooksDetail = (props) => {
    let { id } = useParams()
    const { getDetailBooksDispatch } = props
    const [books, setBooks] = useState()
    useEffect(() => {

        const callData = async () => {
            const response = await getDetailBooksDispatch(id);
            console.log(response)
            if (response.status === 200) {
                setBooks(response.data)
            }

        }
        callData()

    }, [])
    return (
        <>
            <div className="articleDetails">
                <div className="titleDetail">
                    <h1>{books?.title}</h1>
                </div>
                <div className="titleHeighlight">
                    <b>{books?.description}</b>
                </div>
                <div className="contentDetail">
                    {books?.content}
                </div>
                <div className="pictureDetail">
                    <div className="imageDetail" style={{ backgroundImage: `url(${books?.image})` }}></div>
                </div>
            </div>
        </>
    )
}

const mapStateToProps = createStructuredSelector({

})
const mapDispatchtoProps = (dispatch) => ({
    getDetailBooksDispatch: (payload) => getDetailBooksAction(dispatch)(payload),
})

const withConnect = connect(mapStateToProps, mapDispatchtoProps)
export default compose(withConnect)(BooksDetail);


