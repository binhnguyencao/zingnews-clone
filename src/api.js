import axios from "axios";

const URL = "https://62fd0400b9e38585cd4b1834.mockapi.io"

const getListStudentService = () => axios.get(`${URL}/user`)

const createStudentService = (data) => axios.post(`${URL}/user`, data)

const getDetailStudentService = (id) => axios.get(`${URL}/user/${id}`)

const updateStudentService = ({ id, data }) => axios.put(`${URL}/user/${id}`, data)

const deleteCurrentStudentService = (id) => axios.delete(`${URL}/user/${id}`)

export {
    getListStudentService,
    createStudentService,
    getDetailStudentService,
    updateStudentService,
    deleteCurrentStudentService
}